<p>
<h4>Projenin Çalıştırılması</h4>
<ul>
<li>
Proje kurulumu yapıldıktan sonra "php artisan get:orders" komutunu 1 defa çalıştırdığınızda mevcut tüm veriyi kuyruğa alarak içerik aktarma işlemlerini başlatacaktır. Bu içe aktarma tamamlandıktan sonra laravel cronjob bağlantısı yapılmalı. Bu tanımlama yapıldıktan sonra laravel her dakika bu komutu çalıştıracak ve yeni siparişleri aynı kurallar dahilinde içe aktarmaya devam edecektir.</li>
</ul>
</p>
