<?php

class ApiLog {
    public function __construct($url, $httpType, $response)
    {
        \App\Models\ApiLogs::create([
            'url' => $url,
            'http_type' => $httpType,
            'response' => $response,
        ]);
    }
}
