<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApiLogs extends Model
{
    use HasFactory;
    public $incrementing = true;

    protected $table = 'api_logs';

    protected $fillable = [
        'url',
        'http_type',
        'response',
    ];

    protected $casts = [
        'response' => 'array',
    ];
}
