<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;
    public $incrementing = false;

    protected $table = 'orders';

    protected $fillable = [
        'id',
        'payment_method',
        'shipping_method',
        'customer_id',
        'billing_address_id',
        'shipping_address_id',
        'company_id',
        'type',
        'total',
    ];
}
