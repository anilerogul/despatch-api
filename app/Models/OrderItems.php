<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    use HasFactory;

    protected $table = 'order_items';
    public $incrementing = false;

    protected $fillable = [
        'id',
        'order_id',
        'product_id',
        'quantity',
        'subtotal',
    ];
}
