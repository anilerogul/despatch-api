<?php

namespace App\Jobs;

use App\DespatchApi;
use App\Models\Addresses;
use App\Models\Customers;
use App\Models\OrderItems;
use App\Models\Orders;
use App\Models\Products;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ImportOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $order = DespatchApi::getOrder($this->order->id);

            Customers::updateOrCreate(
                [
                    'id' => $order->customer->id
                ],
                Arr::only(
                    (array)$order->customer, [
                    'name',
                    'email',
                    'phone',
                    'created_at',
                    'updated_at'
                ])
            );

            Addresses::updateOrCreate(
                [
                    'id' => $order->shipping_address->id,
                ],
                Arr::only(
                    (array)$order->shipping_address, [
                        'name',
                        'phone',
                        'line_1',
                        'line_2',
                        'city',
                        'country',
                        'state',
                        'postcode',
                        'created_at',
                        'updated_at',
                    ]
                )
            );

            Addresses::updateOrCreate(
                [
                    'id' => $order->billing_address->id,
                ],
                Arr::only(
                    (array)$order->billing_address, [
                        'name',
                        'phone',
                        'line_1',
                        'line_2',
                        'city',
                        'country',
                        'state',
                        'postcode',
                        'created_at',
                        'updated_at',
                    ]
                )
            );

            Orders::updateOrCreate(
                [
                    'id' => $order->id,
                ],
                Arr::only(
                    (array)$order, [
                    'payment_method',
                    'shipping_method',
                    'customer_id',
                    'company_id',
                    'type',
                    'billing_address_id',
                    'shipping_address_id',
                    'total',
                    'created_at',
                    'updated_at'
                ],
                )
            );

            collect($order->order_items)->map(function ($orderItem) {
                Products::updateOrCreate(
                    [
                        'id' => $orderItem->product->id,
                    ],
                    Arr::only(
                        (array)$orderItem->product, [
                            'title',
                            'description',
                            'image',
                            'sku',
                            'price',
                            'created_at',
                            'updated_at'
                        ]
                    )
                );
                OrderItems::updateOrCreate(
                    [
                        'id' => $orderItem->id,
                    ],
                    Arr::only(
                        (array)$orderItem, [
                            'order_id',
                            'product_id',
                            'quantity',
                            'subtotal',
                            'created_at',
                            'updated_at'
                        ]
                    )
                );
            });

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
    }
}
