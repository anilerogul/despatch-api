<?php

namespace App\Jobs;

use App\DespatchApi;
use App\Models\Orders;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class UpdateOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $values;
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $id, array $values)
    {
        $this->values = $values;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $order = DespatchApi::saveOrder($this->id,  $this->values);

            if ($order->failed()) {
                throw new \Exception('Sipariş güncellenemedi.');
            }

            $order = $order->object();

            $o = Orders::find($order->id);
            $o->update(
                Arr::only(
                    (array)$order, [
                    'id',
                    'payment_method',
                    'shipping_method',
                    'customer_id',
                    'company_id',
                    'type',
                    'billing_address_id',
                    'shipping_address_id',
                    'total',
                    'created_at',
                    'updated_at'
                ],
                )
            );
        } catch (\Exception $exception) {
            throw new \Exception('Sipariş güncellenemedi.');
        }
    }
}
