<?php

namespace App\Jobs;

use App\DespatchApi;
use App\Models\Orders;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class GetPage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $page;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $page)
    {
        $this->page = $page;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $last = Orders::orderByDesc('id')->first();

            $orderResponse = $last
                ? DespatchApi::getOrders(['id' => $last->id, 'page' => $this->page])
                : DespatchApi::getOrders(['page' => $this->page]);

            if ($orderResponse->failed()) {
                throw new \Exception($orderResponse->clientError());
            }

            $orderResponse = $orderResponse->object();

            foreach ($orderResponse->data as $innerArray) {
                $queueSize = DB::table('job_batches')->count();

                dispatch(new ImportOrder($innerArray))
                    ->onConnection('database')
                    ->onQueue('default')
                    ->delay(now()->addSeconds($queueSize * 3));

                $queueSize++;

                dispatch(new UpdateOrder($innerArray->id, ['type' => 'approved']))
                    ->onConnection('database')
                    ->onQueue('default')
                    ->delay(now()->addSeconds($queueSize * 3));
            }
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }
}
