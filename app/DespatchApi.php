<?php

namespace App;

use App\Models\ApiLogs;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class DespatchApi
{
    static private $apiUrl = 'https://sample-market.despatchcloud.uk/api/';

    static public function getOrders($params = ['page' => 1]) {
        $params['api_key'] = 'XRy7TT2cY9QDqxlwGZsrbeU1FtwRRrex7cK0oHJTnITrb4p1hPjn2OwHzAcXyoJQTjs85W8uWQjlmraJ91E1qjiWmKNx5VYNhNeHPyIX86kdDtqs4NjNwFBtgzWJUYS';

        $response =  Http::acceptJson()
            ->withoutVerifying()
            ->get(self::$apiUrl . 'orders', $params);

        new \ApiLog(self::$apiUrl . 'orders', 'GET', $response->object());

        return $response;
    }

    static public function getOrder($fromId) {
        $params = [
            'api_key' => 'XRy7TT2cY9QDqxlwGZsrbeU1FtwRRrex7cK0oHJTnITrb4p1hPjn2OwHzAcXyoJQTjs85W8uWQjlmraJ91E1qjiWmKNx5VYNhNeHPyIX86kdDtqs4NjNwFBtgzWJUYS'
        ];

        $response = Http::acceptJson()
            ->withoutVerifying()
            ->get(self::$apiUrl . 'orders/' . $fromId, $params)
            ->object();

        new \ApiLog(self::$apiUrl . 'orders/', 'GET', $response->object());

        return $response->object();
    }

    static public function saveOrder($fromId, $params) {
        $apiKey = [
            'api_key' => 'XRy7TT2cY9QDqxlwGZsrbeU1FtwRRrex7cK0oHJTnITrb4p1hPjn2OwHzAcXyoJQTjs85W8uWQjlmraJ91E1qjiWmKNx5VYNhNeHPyIX86kdDtqs4NjNwFBtgzWJUYS'
        ];

        $params = array_merge($apiKey, $params);

        $response = Http::acceptJson()
            ->withoutVerifying()
            ->post(self::$apiUrl . 'orders/' . $fromId, $params);

        new \ApiLog(self::$apiUrl . 'orders/' . $fromId, 'POST', $response->object());

        return $response;
    }

}
