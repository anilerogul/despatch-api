<?php

namespace App\Console\Commands;

use App\DespatchApi;
use App\Jobs\GetPage;
use App\Jobs\ImportOrder;
use App\Jobs\UpdateOrder;
use App\Models\Orders;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GetOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $last = Orders::orderByDesc('id')->first();

        $firstPageData = $last
            ? DespatchApi::getOrders(['id' => $last->id, 'page' => 1])->object()
            : DespatchApi::getOrders(['page' => 1])->object();

        if ($firstPageData->total > 0) {
            for ($i = 1; $i <= $firstPageData->last_page; $i++) {
                $queueSize = DB::table('job_batches')->count();

                dispatch(new GetPage($i))
                    ->onConnection('database')
                    ->onQueue('default')
                    ->delay(now()->addSeconds($queueSize * 3));
            }
        }

        return Command::SUCCESS;
    }


    /**
     * The job failed to process.
     *
     * @param \Exception $exception
     *
     * @return void
     * @throws \Exception
     */
    public function failed(\Exception $exception)
    {
        throw new \Exception($exception->getMessage(), 500);
    }
}
